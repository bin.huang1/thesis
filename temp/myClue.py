import pandas as pd
import sys


def helper(given_path, output_path, store_path,csize=10):
    given = pd.read_csv(given_path,delimiter = "\t")
    output = pd.read_csv(output_path, delimiter = "\t",chunksize=csize)

    # create a dictionary to save search time
    m = dict()
    for idx, row in given.iterrows():
        m[row['#TaxID']] = (row['Level'], row['Taxonomy'])
    idx = 1
    for chunk in output:
        print('Now working on the {}_th chunk'.format(idx))
        Level, Taxonomy = [], []
        for i, row in chunk.iterrows():
            if row['taxid'] in m:
                Level.append(m[row['taxid']][0])
                Taxonomy.append(m[row['taxid']][1])
            else:
                Level.append('NaN')
                Taxonomy.append('NaN')
        chunk['Level'] = Level
        chunk['Taxonomy'] = Taxonomy
        chunk.to_csv(store_path+'chunk_{}.txt'.format(idx))
        idx += 1
    print('all done')


def main():
  # assert len(sys.argv) == 5, "Usage: helper.py given_file_path, output_file_path, store_path, chunksize"
  g = sys.argv[1]
  o = sys.argv[2]
  s = sys.argv[3]
  c = int(sys.argv[4])
  helper(g,o,s,c)

if __name__ == '__main__':
    main()
