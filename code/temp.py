"""
Python model "demo_sdm.py"
Translated using PySD version 0.10.0
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME': 'time',
    'Time': 'time',
    'Process active time1D': 'process_active_time1d',
    'Waiting time in process per case1D': 'waiting_time_in_process_per_case1d',
    'Time in process per case1D': 'time_in_process_per_case1d',
    'Service time per case1D': 'service_time_per_case1d',
    'Finish rate1D': 'finish_rate1d',
    'Arrival rate1D': 'arrival_rate1d',
    'Num in process case1D': 'num_in_process_case1d',
    'Num of unique resource1D': 'num_of_unique_resource1d',
    'FINAL TIME': 'final_time',
    'INITIAL TIME': 'initial_time',
    'SAVEPER': 'saveper',
    'TIME STEP': 'time_step'
}

__pysd_version__ = "0.10.0"

__data = {'scope': None, 'time': lambda: 0}


def _init_outer_references(data):
    for key in data:
        __data[key] = data[key]


def time():
    return __data['time']()


@cache('step')
def process_active_time1d():
    """
    Real Name: b'Process active time1D'
Original Eqn: b'123.83645345109805 * arrival_rate1d()+ 25.544715247279864 * finish_rate1d() + 164.19902207779162 * num_of_unique_resource1d() + 0.18625454172504824 * service_time_per_case1d() + 9.743396928265826 * num_in_process_case1d() -3802.945'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 123.83645345109805 * arrival_rate1d()+ 25.544715247279864 * finish_rate1d() + 164.19902207779162 * num_of_unique_resource1d() + 0.18625454172504824 * service_time_per_case1d() + 9.743396928265826 * num_in_process_case1d() -3802.945

@cache('run')
def waiting_time_in_process_per_case1d():
    """
    Real Name: b'Waiting time in process per case1D'
    Original Eqn: b'0'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 0


@cache('step')
def time_in_process_per_case1d():
    """
    Real Name: b'Time in process per case1D'
Original Eqn: b'-1.003406238018099 * arrival_rate1d() -1.5046581186935821 * num_of_unique_resource1d() + 0.0054015753529381705 * process_active_time1d() + 0.033674414814116774 * service_time_per_case1d() +191.804'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -1.003406238018099 * arrival_rate1d() -1.5046581186935821 * num_of_unique_resource1d() + 0.0054015753529381705 * process_active_time1d() + 0.033674414814116774 * service_time_per_case1d() +191.804    ) + 0.01 * process_active_time1d()


@cache('step')
def service_time_per_case1d():
    """
    Real Name: b'Service time per case1D'
Original Eqn: b'66.40160586697098 * num_of_unique_resource1d()-345.058'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 66.40160586697098 * num_of_unique_resource1d()-345.058    ) + 1.2 * time_in_process_per_case1d() + 0.1 * process_active_time1d()


@cache('step')
def finish_rate1d():
    """
    Real Name: b'Finish rate1D'
Original Eqn: b'0.9530629623326526 * arrival_rate1d()+ 1.1468984268415774 * num_of_unique_resource1d()  -0.005541249149232541 * service_time_per_case1d()  -0.310221629036053 * num_in_process_case1d() -6.964'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 0.9530629623326526 * arrival_rate1d()+ 1.1468984268415774 * num_of_unique_resource1d()  -0.005541249149232541 * service_time_per_case1d()  -0.310221629036053 * num_in_process_case1d() -6.964

@cache('run')
def arrival_rate1d():
    """
    Real Name: b'Arrival rate1D'
    Original Eqn: b'1'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 1


@cache('step')
def num_in_process_case1d():
    """
    Real Name: b'Num in process case1D'
    Original Eqn: b'INTEG ( Arrival rate1D-Finish rate1D, 1)'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return _integ_num_in_process_case1d()


@cache('run')
def num_of_unique_resource1d():
    """
    Real Name: b'Num of unique resource1D'
    Original Eqn: b'50'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 50


@cache('run')
def final_time():
    """
    Real Name: b'FINAL TIME'
    Original Eqn: b'33'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The final time for the simulation.'
    """
    return 33


@cache('run')
def initial_time():
    """
    Real Name: b'INITIAL TIME'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The initial time for the simulation.'
    """
    return 1


@cache('step')
def saveper():
    """
    Real Name: b'SAVEPER'
    Original Eqn: b'TIME STEP'
    Units: b'Day'
    Limits: (0.0, None)
    Type: component

    b'The frequency with which output is stored.'
    """
    return time_step()


@cache('run')
def time_step():
    """
    Real Name: b'TIME STEP'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (0.0, None)
    Type: constant

    b'The time step for the simulation.'
    """
    return 1


_integ_num_in_process_case1d = functions.Integ(lambda: arrival_rate1d() - finish_rate1d(),
                                               lambda: 1)
