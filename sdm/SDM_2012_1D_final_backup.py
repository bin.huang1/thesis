"""
Python model "SDM_2012_1D_final_backup.py"
Translated using PySD version 0.10.0
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME': 'time',
    'Time': 'time',
    'Finish rate': 'finish_rate',
    'Service time per case': 'service_time_per_case',
    'Time in process per case': 'time_in_process_per_case',
    'Arrival rate': 'arrival_rate',
    'Num of unique resource': 'num_of_unique_resource',
    'Num in process per case': 'num_in_process_per_case',
    'FINAL TIME': 'final_time',
    'INITIAL TIME': 'initial_time',
    'SAVEPER': 'saveper',
    'TIME STEP': 'time_step'
}

__pysd_version__ = "0.10.0"

__data = {'scope': None, 'time': lambda: 0}


def _init_outer_references(data):
    for key in data:
        __data[key] = data[key]


def time():
    return __data['time']()


@cache('step')
def finish_rate():
    """
    Real Name: b'Finish rate'
    Original Eqn: b'0.953*Arrival rate+1.147*Num of unique resource-21.616-22.3'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return 0.953 * arrival_rate() + 1.147 * num_of_unique_resource() - 21.616 - 22.3


@cache('step')
def service_time_per_case():
    """
    Real Name: b'Service time per case'
    Original Eqn: b'-3.104*Finish rate+12.362*Time in process per case+66.402*Num of unique resource-2755.11'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return -3.104 * finish_rate() + 12.362 * time_in_process_per_case(
    ) + 66.402 * num_of_unique_resource() - 2755.11


@cache('step')
def time_in_process_per_case():
    """
    Real Name: b'Time in process per case'
    Original Eqn: b'-1.003*Arrival rate-1.505*Num of unique resource+191.804+167'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -1.003 * arrival_rate() - 1.505 * num_of_unique_resource() + 191.804 + 167


@cache('run')
def arrival_rate():
    """
    Real Name: b'Arrival rate'
    Original Eqn: b'15'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 15


@cache('run')
def num_of_unique_resource():
    """
    Real Name: b'Num of unique resource'
    Original Eqn: b'10'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 10


@cache('step')
def num_in_process_per_case():
    """
    Real Name: b'Num in process per case'
    Original Eqn: b'INTEG ( Arrival rate-Finish rate, 12)'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return _integ_num_in_process_per_case()


@cache('run')
def final_time():
    """
    Real Name: b'FINAL TIME'
    Original Eqn: b'152'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The final time for the simulation.'
    """
    return 152


@cache('run')
def initial_time():
    """
    Real Name: b'INITIAL TIME'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The initial time for the simulation.'
    """
    return 1


@cache('step')
def saveper():
    """
    Real Name: b'SAVEPER'
    Original Eqn: b'TIME STEP'
    Units: b'Day'
    Limits: (0.0, None)
    Type: component

    b'The frequency with which output is stored.'
    """
    return time_step()


@cache('run')
def time_step():
    """
    Real Name: b'TIME STEP'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (0.0, None)
    Type: constant

    b'The time step for the simulation.'
    """
    return 1


_integ_num_in_process_per_case = functions.Integ(lambda: arrival_rate() - finish_rate(),
                                                 lambda: 12)
