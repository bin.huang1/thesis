"""
Python model "demo_sdm_2h.py"
Translated using PySD version 0.10.0
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME': 'time',
    'Time': 'time',
    'Process active time1D': 'process_active_time1d',
    'Waiting time in process per case1D': 'waiting_time_in_process_per_case1d',
    'Time in process per case1D': 'time_in_process_per_case1d',
    'Service time per case1D': 'service_time_per_case1d',
    'Finish rate1D': 'finish_rate1d',
    'Arrival rate1D': 'arrival_rate1d',
    'Num in process case1D': 'num_in_process_case1d',
    'Num of unique resource1D': 'num_of_unique_resource1d',
    'FINAL TIME': 'final_time',
    'INITIAL TIME': 'initial_time',
    'SAVEPER': 'saveper',
    'TIME STEP': 'time_step'
}

__pysd_version__ = "0.10.0"

__data = {'scope': None, 'time': lambda: 0}


def _init_outer_references(data):
    for key in data:
        __data[key] = data[key]


def time():
    return __data['time']()


@cache('step')
def process_active_time1d():
    """
    Real Name: b'Process active time1D'
Original Eqn: b'230.0930744658604 * arrival_rate1d()+ 3.562131191552225 * finish_rate1d() + 83.48724446582933 * num_of_unique_resource1d() + 0.496627356079453 * service_time_per_case1d() + 47.35668823805592 * time_in_process_per_case1d() + 0.03157452498624419 * num_in_process_case1d() -15783.031'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 230.0930744658604 * arrival_rate1d()+ 3.562131191552225 * finish_rate1d() + 83.48724446582933 * num_of_unique_resource1d() + 0.496627356079453 * service_time_per_case1d() + 47.35668823805592 * time_in_process_per_case1d() + 0.03157452498624419 * num_in_process_case1d() -15783.031
@cache('run')
def waiting_time_in_process_per_case1d():
    """
    Real Name: b'Waiting time in process per case1D'
    Original Eqn: b'0'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 0


@cache('step')
def time_in_process_per_case1d():
    """
    Real Name: b'Time in process per case1D'
Original Eqn: b'-2.7880468420746376 * arrival_rate1d() -0.5458298502917718 * num_of_unique_resource1d()  -0.005823384701862512 * num_in_process_case1d() +491.841'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -2.7880468420746376 * arrival_rate1d() -0.5458298502917718 * num_of_unique_resource1d()  -0.005823384701862512 * num_in_process_case1d() +491.841

@cache('step')
def service_time_per_case1d():
    """
    Real Name: b'Service time per case1D'
Original Eqn: b'-5.54924273654864 * arrival_rate1d()+ 75.9942634127108 * num_of_unique_resource1d() + 6.03781859122251 * time_in_process_per_case1d()  -1.4798508793454104 * num_in_process_case1d() -1967.720'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -5.54924273654864 * arrival_rate1d()+ 75.9942634127108 * num_of_unique_resource1d() + 6.03781859122251 * time_in_process_per_case1d()  -1.4798508793454104 * num_in_process_case1d() -1967.720

@cache('step')
def finish_rate1d():
    """
    Real Name: b'Finish rate1D'
Original Eqn: b'-0.009399503578900358 * service_time_per_case1d() -0.15710491072262722 * time_in_process_per_case1d() +125.800'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -0.009399503578900358 * service_time_per_case1d() -0.15710491072262722 * time_in_process_per_case1d() +125.800
@cache('run')
def arrival_rate1d():
    """
    Real Name: b'Arrival rate1D'
Original Eqn: b'36'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 36
@cache('step')
def num_in_process_case1d():
    """
    Real Name: b'Num in process case1D'
    Original Eqn: b'INTEG ( Arrival rate1D-Finish rate1D, 1)'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return _integ_num_in_process_case1d()


@cache('run')
def num_of_unique_resource1d():
    """
    Real Name: b'Num of unique resource1D'
Original Eqn: b'37'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 37
@cache('run')
def final_time():
    """
    Real Name: b'FINAL TIME'
    Original Eqn: b'152'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The final time for the simulation.'
    """
    return 152

@cache('run')
def initial_time():
    """
    Real Name: b'INITIAL TIME'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The initial time for the simulation.'
    """
    return 1

@cache('step')
def saveper():
    """
    Real Name: b'SAVEPER'
    Original Eqn: b'TIME STEP'
    Units: b'Day'
    Limits: (0.0, None)
    Type: component

    b'The frequency with which output is stored.'
    """
    return time_step()


@cache('run')
def time_step():
    """
    Real Name: b'TIME STEP'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (0.0, None)
    Type: constant

    b'The time step for the simulation.'
    """
    return 1


_integ_num_in_process_case1d = functions.Integ(lambda: arrival_rate1d() - finish_rate1d(),
                                               lambda: 1)
