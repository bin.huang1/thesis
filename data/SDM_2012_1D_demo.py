"""
Python model "SDM_2012_1D_demo.py"
Translated using PySD version 0.10.0
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME': 'time',
    'Time': 'time',
    'Finish rate1D': 'finish_rate1d',
    'Service time per case1D': 'service_time_per_case1d',
    'Time in process per case1D': 'time_in_process_per_case1d',
    'Arrival rate1D': 'arrival_rate1d',
    'Num of unique resource1D': 'num_of_unique_resource1d',
    'Num in process case1D': 'num_in_process_case1d',
    'FINAL TIME': 'final_time',
    'INITIAL TIME': 'initial_time',
    'SAVEPER': 'saveper',
    'TIME STEP': 'time_step'
}

__pysd_version__ = "0.10.0"

__data = {'scope': None, 'time': lambda: 0}


def _init_outer_references(data):
    for key in data:
        __data[key] = data[key]


def time():
    return __data['time']()


@cache('step')
def finish_rate1d():
    """
    Real Name: b'Finish rate1D'
    Original Eqn: b'1.147*Num of unique resource1D-10'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return 1.147 * num_of_unique_resource1d() - 10


@cache('step')
def service_time_per_case1d():
    """
    Real Name: b'Service time per case1D'
    Original Eqn: b'-2*Finish rate1D+12.362*Time in process per case1D-5*Num of unique resource1D'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return -2 * finish_rate1d() + 12.362 * time_in_process_per_case1d(
    ) - 5 * num_of_unique_resource1d()


@cache('step')
def time_in_process_per_case1d():
    """
    Real Name: b'Time in process per case1D'
    Original Eqn: b'1.003*Arrival rate1D-1.505*Num of unique resource1D+200'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 1.003 * arrival_rate1d() - 1.505 * num_of_unique_resource1d() + 200


@cache('run')
def arrival_rate1d():
    """
    Real Name: b'Arrival rate1D'
    Original Eqn: b'15'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 15


@cache('run')
def num_of_unique_resource1d():
    """
    Real Name: b'Num of unique resource1D'
    Original Eqn: b'10'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 10


@cache('step')
def num_in_process_case1d():
    """
    Real Name: b'Num in process case1D'
    Original Eqn: b'INTEG ( Arrival rate1D-Finish rate1D, 12)'
    Units: b''
    Limits: (0.0, None)
    Type: component

    b''
    """
    return _integ_num_in_process_case1d()


@cache('run')
def final_time():
    """
    Real Name: b'FINAL TIME'
    Original Eqn: b'152'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The final time for the simulation.'
    """
    return 152


@cache('run')
def initial_time():
    """
    Real Name: b'INITIAL TIME'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (None, None)
    Type: constant

    b'The initial time for the simulation.'
    """
    return 1


@cache('step')
def saveper():
    """
    Real Name: b'SAVEPER'
    Original Eqn: b'TIME STEP'
    Units: b'Day'
    Limits: (0.0, None)
    Type: component

    b'The frequency with which output is stored.'
    """
    return time_step()


@cache('run')
def time_step():
    """
    Real Name: b'TIME STEP'
    Original Eqn: b'1'
    Units: b'Day'
    Limits: (0.0, None)
    Type: constant

    b'The time step for the simulation.'
    """
    return 1


_integ_num_in_process_case1d = functions.Integ(lambda: arrival_rate1d() - finish_rate1d(),
                                               lambda: 12)
