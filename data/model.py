"""
Python model "model.py"
Translated using PySD version 0.10.0
"""
from __future__ import division
import numpy as np
from pysd import utils
import xarray as xr

from pysd.py_backend.functions import cache
from pysd.py_backend import functions

_subscript_dict = {}

_namespace = {
    'TIME': 'time',
    'Time': 'time',
    'arrival_rate1w': 'arrival_rate1w',
    'finish_rate1w': 'finish_rate1w',
    'process_active_time1w': 'process_active_time1w',
    'time_in_process_per_case1w': 'time_in_process_per_case1w',
    'waiting_time_in_process_per_case1w': 'waiting_time_in_process_per_case1w',
    'num_in_process_case1w': 'num_in_process_case1w',
    'service_time_per_case1w': 'service_time_per_case1w',
    'num_of_unique_resource1w': 'num_of_unique_resource1w',
    'FINAL TIME': 'final_time',
    'INITIAL TIME': 'initial_time',
    'SAVEPER': 'saveper',
    'TIME STEP': 'time_step'
}

__pysd_version__ = "0.10.0"

__data = {'scope': None, 'time': lambda: 0}


def _init_outer_references(data):
    for key in data:
        __data[key] = data[key]


def time():
    return __data['time']()


@cache('step')
def arrival_rate1w():
    """
    Real Name: b'arrival_rate1w'
    Original Eqn: b'-0.5282063 * num_of_unique_resource1w + -0.45473603 * service_time_per_case1w + -0.6990708 * time_in_process_per_case1w + -0.24433476 * waiting_time_in_process_per_case1w + 0.00516066 * num_in_process_case1w + 1129.0974493955032'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -0.5282063 * num_of_unique_resource1w() + -0.45473603 * service_time_per_case1w(
    ) + -0.6990708 * time_in_process_per_case1w(
    ) + -0.24433476 * waiting_time_in_process_per_case1w() + 0.00516066 * num_in_process_case1w(
    ) + 1129.0974493955032


@cache('step')
def finish_rate1w():
    """
    Real Name: b'finish_rate1w'
    Original Eqn: b'-0.67648767 * arrival_rate1w + 2.82675527 * num_of_unique_resource1w + -0.369938107 * service_time_per_case1w + -0.397416426 * time_in_process_per_case1w + -0.0274783189 * waiting_time_in_process_per_case1w + 0.381376692 * num_in_process_case1w + 302.64495590450326'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -0.67648767 * arrival_rate1w() + 2.82675527 * num_of_unique_resource1w(
    ) + -0.369938107 * service_time_per_case1w() + -0.397416426 * time_in_process_per_case1w(
    ) + -0.0274783189 * waiting_time_in_process_per_case1w() + 0.381376692 * num_in_process_case1w(
    ) + 302.64495590450326


@cache('step')
def process_active_time1w():
    """
    Real Name: b'process_active_time1w'
    Original Eqn: b'0.333333333 * arrival_rate1w * service_time_per_case1w + 0.666666667 * arrival_rate1w * time_in_process_per_case1w + 0.333333333 * arrival_rate1w * waiting_time_in_process_per_case1w + 0.016214403541412847'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 0.333333333 * arrival_rate1w() * service_time_per_case1w(
    ) + 0.666666667 * arrival_rate1w() * time_in_process_per_case1w(
    ) + 0.333333333 * arrival_rate1w() * waiting_time_in_process_per_case1w() + 0.016214403541412847


@cache('step')
def time_in_process_per_case1w():
    """
    Real Name: b'time_in_process_per_case1w'
    Original Eqn: b'1.0 * service_time_per_case1w + 1.0 * waiting_time_in_process_per_case1w + 0'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return 1.0 * service_time_per_case1w() + 1.0 * waiting_time_in_process_per_case1w() + 0


@cache('step')
def waiting_time_in_process_per_case1w():
    """
    Real Name: b'waiting_time_in_process_per_case1w'
    Original Eqn: b'-1.0 * service_time_per_case1w + 1.0 * 523.4869462001786 + 0'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return -1.0 * service_time_per_case1w() + 1.0 * 523.4869462001786 + 0


@cache('step')
def num_in_process_case1w():
    """
    Real Name: b'num_in_process_case1w'
    Original Eqn: b'INTEG(1 * arrival_rate1w + -1 * finish_rate1w , 105.0)'
    Units: b''
    Limits: (None, None)
    Type: component

    b''
    """
    return _integ_num_in_process_case1w()


@cache('run')
def service_time_per_case1w():
    """
    Real Name: b'service_time_per_case1w'
    Original Eqn: b'0'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 0


@cache('run')
def num_of_unique_resource1w():
    """
    Real Name: b'num_of_unique_resource1w'
    Original Eqn: b'0'
    Units: b''
    Limits: (None, None)
    Type: constant

    b''
    """
    return 0


@cache('run')
def final_time():
    """
    Real Name: b'FINAL TIME'
    Original Eqn: b'53'
    Units: b''
    Limits: (None, None)
    Type: constant

    b'The final time for the simulation.'
    """
    return 53


@cache('run')
def initial_time():
    """
    Real Name: b'INITIAL TIME'
    Original Eqn: b'1'
    Units: b''
    Limits: (None, None)
    Type: constant

    b'The initial time for the simulation.'
    """
    return 1


@cache('step')
def saveper():
    """
    Real Name: b'SAVEPER'
    Original Eqn: b'TIME STEP'
    Units: b''
    Limits: (None, None)
    Type: component

    b'The frequency with which output is stored.'
    """
    return time_step()


@cache('run')
def time_step():
    """
    Real Name: b'TIME STEP'
    Original Eqn: b'1'
    Units: b''
    Limits: (None, None)
    Type: constant

    b'The time step for the simulation.'
    """
    return 1


_integ_num_in_process_case1w = functions.Integ(lambda: 1 * arrival_rate1w() + -1 * finish_rate1w(),
                                               lambda: 105.0)
