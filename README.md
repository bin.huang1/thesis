**Instructions on how to run this framework**

The context follows the order of the project's workflow.

- Part 1. Get the best equation for selected process variable in given SD-log
    1. Call the class 'InterFramework'
    `obj = InterFramework()`
    2. Read the given SD-Log `data = obj.read_sd(path_to_file)`
    3. Drop whatever variable you don't like(for example, the variable(s) that seems non-sense/trivial in the given SD. The variable 'Waiting time in  process per case1D' has been drop beacase it only contains negative values which are obtained by subtracting value of 'Service time per case1D' from 'Time in process per case1D').\
    `test_df = obj.drop_variables(data,'Waiting time in process per case1D')`
    
    4. Run the framework `obj.call_framework(test_df, process_var = None)` \
        If the process_var is not given, then the framework will try to find the best equation for every process variable in the given SD-Log, otherwise, it will only return the best equation for selected process variable
    5. Follow the instructions to run the framework
    6. the all best equation are in the container `all_best_equations = obj.best_equation`


- Part 2. To run a system dynamics model
    1. Specify the stock and flow variables: `sf_vars = GetInteraction.get_stocks_and_flows(test_df)`
    2. Specify the data variable, i,e, variables, whose values will be read directly from local. `data_var = GetInteraction.get_data_variables(test_df)`
    3. Processing the equations to
        1. remove variables with tiny coefficient to save computational effort and also simplify the final SDM model `g = equation_processing.Equationprocessing.process_equations(test_df, all_best_equations, sf_vars, data_var)`       
        2. detect and remove cycles if necessary:\
`lines = equation_processing.Equationprocessing.lines_info(g, sf_vars)`\
`updated_g = equation_processing.Equationprocessing.remove_cycles(lines, g, test_df)`
        3. rebuild the processed eqution and forward it into a pre-defined SDM model. \
        `equations_in_SD = GetBestModel.rebuild_equations(updated_g)`\
        `update_model = UpdateSDM.update_sdm(test_df, equations_in_SD,sf_vars,path_to_sdm)`

- Part 3. Check the results
    1. run previous update model to get the simulated results:
    `stocks = UpdateSDM.run_sdm(update_model, data_var,test_df)`
    2. Plot the results for inspection:
    `PlotSimulatedresult.plot_simulated_res(stocks,test_df)`



**Mark:**
- For convenience, the `all_best_equations` are not human-read friendly (a list contains coefficients, variables and so on).\
The mathematical formula will be automatically saved in the desktop folder, also the processed SD-Log.

- The variable name in the given SDM model should have the same name with variables in the given SD-Log

- In Part 3, when check the result. It will compare simulated values and raw values in the SD-Log:
    - Since the data variables read directly from local, so the simulated values and raw values are identical
    - For test case 'Active2012_1D_sdlog.csv', the results are not acceptable but it may because the data are made intentional, because for following variables:
        `Time in process per case1D`, `Waiting time in process per case1D` and `Service time per case1D`\
        They have a relation : `Waiting time in process per case1D` = `Time in process per case1D` - `Service time per case1D`\
        But in a SDM, we must break one to run the model...
    - For other dataset I tested with, most of them have a good simulation results. 
